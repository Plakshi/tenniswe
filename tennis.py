
def reset(d :dict) -> dict :
	d[PLAYER1] = 0
	d[PLAYER2] = 0
	return d

class ScoreBoard:
	def __init__(self):
		self.score = {'A': 0, 'B':0}
		self.game = {'A': 0, 'B': 0}
		self.set = {'A': 0, 'B': 0}
	def __str__(self) -> dict:
		return str(self.score)
	
